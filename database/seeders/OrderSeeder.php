<?php

namespace Database\Seeders;

use App\Models\Client;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class OrderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1; $i<100; $i++) {
            $client_id = Client::query()->inRandomOrder()->first()->id;
            $order_id = DB::table('orders')->insertGetId([
                'client_id' => $client_id,
                'created_at'=>date('Y-m-d H:s:i')
            ]);
            DB::table('transactions')->insert([
                'client_id' => $client_id,
                'order_id' => $order_id,
                'total_sum' => rand(100, 10000),
                'created_at'=>date('Y-m-d H:s:i')
            ]);
        }
    }
}
