<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Models\Client;
use App\Models\Transaction;
use Illuminate\Http\Request;

class TransactionController extends Controller
{
    public function get_client_transactions(Request $request)
    {
        if (!$request->has('client_id')){
            return response()->json([
                "success"=>false,
                "message"=>"Client ID is empty"
            ],400);
        }

        $client = Client::find($request->client_id);
        if ($client == null){
            return response()->json([
                "success"=>false,
                "message"=>"Client not found"
            ],400);
        }

        $data = Transaction::with(['transaction_order'])
            ->where('client_id','=',$request->client_id)
            ->paginate(20);

        return response()->json([
            "success"=>true,
            "data"=>$data
        ]);
    }
}
