<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;
    protected $table='transactions';
    protected $fillable = [
        'client_id',
        'order_id',
        'total_sum',
    ];

    public function transaction_order(){
        return $this->belongsTo(Order::class,'order_id','id');
    }
}
